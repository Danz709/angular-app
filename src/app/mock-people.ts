import { Person } from './person'

export const PEOPLE: Person[] = [
    { id: 1, name: "male1", gender: "m"},
    { id: 2, name: "male2", gender: "m"},
    { id: 3, name: "male3", gender: "m"},
    { id: 4, name: "female1", gender: "f"},
    { id: 5, name: "female2", gender: "f"},
    { id: 6, name: "female3", gender: "f"},
]