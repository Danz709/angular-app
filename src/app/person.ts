export class Person {
    id: number;
    name: string;
    gender: string;
}